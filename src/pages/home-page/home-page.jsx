import React from 'react';
import { connect } from 'react-redux';

import MoviePreview from '../../components/movie-preview/movie-preview';
import Header from '../../components/header/header';
import { filterMoviesByLanguage, getMovieDetails, clearMovieDetails, filterMoviesByGenre, filterMoviesBySearch, resetFilter } from '../../actions/filterActions';

class HomePage extends React.Component {

  handleLanguageFilter = (selectedLanguage) => {
    this.props.filterMoviesByLanguage(selectedLanguage)
  }

  handleSelectedMovie = (selectedMovie,order) => {
    this.props.getMovieDetails(selectedMovie,order);
  }

  clearMovieDetails = () => {
    this.props.clearMovieDetails();
  }

  handleSelectedGenre = (selectedGenre) => {
    this.props.filterMoviesByGenre(selectedGenre)
  }

  handleSearchChange = (event) => {
    this.props.filterMoviesBySearch(event.target.value)
  }

  handleClose = () => {
    this.props.resetFilter();
  }

  render() {
    const { movieData } = this.props;
    return (
      <div>
        <Header 
          languageOptions={movieData.languageOptions} 
          handleLanguageFilter={this.handleLanguageFilter} 
          selectedLanguage={movieData.selectedLanguage}
          genreOptions={movieData.genreOptions}
          selectedGenre={movieData.selectedGenre}
          handleSelectedGenre={this.handleSelectedGenre}
          search={movieData.search}
          handleSearchChange={this.handleSearchChange}
        />
        <MoviePreview 
          movies={movieData.movieCollection} 
          handleSelectedMovie={this.handleSelectedMovie} 
          selectedMovieDetail={movieData.selectedMovie}
          clearMovieDetails={this.clearMovieDetails}
          selectedGenre={movieData.selectedGenre}
          search={movieData.search}
          selectedLanguage={movieData.selectedLanguage}
          handleClose={this.handleClose}
          selectedMovieOrder={movieData.selectedMovieOrder}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  movieData: state.movieCollection
})

export default connect(mapStateToProps,{ 
  filterMoviesByLanguage, 
  getMovieDetails, 
  clearMovieDetails,
  filterMoviesByGenre,
  filterMoviesBySearch,
  resetFilter
})(HomePage);

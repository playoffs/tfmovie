import MOVIE_DATA from './movieData';
import { FILTER_BY_LANGUAGE, RESET_FILTER, SELECTED_MOVIE ,CLEAR_SELECTED_MOVIE, FILTER_BY_GENRE, FILTER_BY_SEARCH } from '../actions/types';

const initialState = {
  languageOptions: MOVIE_DATA[0],
  movieCollection: MOVIE_DATA[1],
  selectedLanguage: '',
  selectedMovie: {},
  genreOptions: [
    'Action',
    'Adventure',
    'Animation',
    'Biography',
    'Comedy',
    'Crime',
    'Documentary',
    'Drama',
    'Family',
    'Fantasy',
    'Film-Noir',
    'Game-Show',
    'History',
    'Horror',
    'Music',
    'Musical',
    'Mystery',
    'News',
    'Reality-TV',
    'Romance',
    'Sci-Fi',
    'Sport',
    'Talk-Show',
    'Thriller',
    'War',
    'wWestern'
  ],
  selectedGenre: [],
  search: '',
  selectedMovieOrder: ''
}

export default function(state=initialState,action) {
  switch (action.type) {
    case FILTER_BY_LANGUAGE:
      let filterLanguage = Object.keys(state.movieCollection)
        .filter(key => state.movieCollection[key].EventLanguage === action.payload)
        .reduce((obj, key) => {
          obj[key] = state.movieCollection[key];
          return obj;
        }, {});
      return {
        ...state,
        movieCollection: filterLanguage,
        selectedLanguage: action.payload
      }  
    case SELECTED_MOVIE:
      let movieOrder = Math.trunc(action.payload[1]/6);
      let selectedMovieOrder = movieOrder * 6;  
      const selectedMovie = Object.keys(state.movieCollection)
        .filter(key => state.movieCollection[key].EventGroup === action.payload[0])
        .reduce((obj,key) => {
          obj = state.movieCollection[key];
          return obj;
        }, {});
      return {
        ...state,
        selectedMovie: selectedMovie,
        selectedMovieOrder: selectedMovieOrder    
      }
    case FILTER_BY_GENRE:
      let genre = action.payload.toString().replace(',','|');
      let filterGenre = Object.keys(state.movieCollection)
        .filter(key => state.movieCollection[key].EventGenre.includes(genre))
        .reduce((obj,key) => {
          obj[key] = state.movieCollection[key];
          return obj;
        }, {});
        return {
          ...state,
          movieCollection: filterGenre,
          selectedGenre: action.payload
        }   
    case FILTER_BY_SEARCH:
      let search = action.payload.toUpperCase();
      let filterSearch = Object.keys(state.movieCollection)
      .filter(key => state.movieCollection[key].EventTitle.toUpperCase().indexOf(search) > -1)
      .reduce((obj, key) => {
        obj[key] = state.movieCollection[key];
        return obj;
      }, {});
      return {
        ...state,
        movieCollection: filterSearch,
        search: action.payload
      }  
    case RESET_FILTER:
      return initialState; 
    case CLEAR_SELECTED_MOVIE:
      return {
        ...state,
        selectedMovie: {}
      }   
    default:
      return state;
  }
}
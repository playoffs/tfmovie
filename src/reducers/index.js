import { combineReducers } from 'redux';

import movieReducer from './movieReducer';

const rootReducer = combineReducers({
  movieCollection: movieReducer
})

export default rootReducer;
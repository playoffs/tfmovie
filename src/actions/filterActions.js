import { FILTER_BY_LANGUAGE, RESET_FILTER, SELECTED_MOVIE, CLEAR_SELECTED_MOVIE, FILTER_BY_GENRE, FILTER_BY_SEARCH } from './types';

export const filterMoviesByLanguage = ( language ) => dispatch => {
  dispatch({
    type: RESET_FILTER
  })

  dispatch({
    type: FILTER_BY_LANGUAGE,
    payload: language
  })
}  

export const getMovieDetails = (selectedMovie,order) => dispatch => {
  dispatch({
    type: SELECTED_MOVIE,
    payload: [selectedMovie,order]
  })
}

export const clearMovieDetails = () => dispatch => {
  dispatch({
    type: CLEAR_SELECTED_MOVIE
  })
}

export const filterMoviesByGenre = ( genre ) => dispatch => {
  dispatch({
    type: RESET_FILTER
  })

  dispatch({
    type: FILTER_BY_GENRE,
    payload: genre
  })
}

export const filterMoviesBySearch = ( search ) => dispatch => {
  dispatch({
    type: RESET_FILTER
  })
  
  dispatch({
    type: FILTER_BY_SEARCH,
    payload: search
  })
}

export const resetFilter = () => dispatch => {
  dispatch({
    type: RESET_FILTER
  })
}

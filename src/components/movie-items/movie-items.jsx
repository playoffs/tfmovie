import React from 'react'
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';

import { config } from '../../config/config';
import './movie-items.scss';
import MovieDetail from '../movie-detail/movie-detail';

const MovieItens = ({ movie, handleSelectedMovie, selectedMovie, clearMovieDetails, order, selectedMovieOrder }) => {
  const { EventGroup, EventTitle, EventCode, wtsPerc, wtsCount, ShowDate } = movie;
  let releaseDate = ShowDate.split(',');
  
  return (
    <>
    {(Object.keys(selectedMovie).length && selectedMovie.EventGroup === EventGroup) ? <MovieDetail selectedMovie={selectedMovie} order={selectedMovieOrder} clearMovieDetails={clearMovieDetails} /> : null}
    <div style={{order: selectedMovieOrder === order ? order + 1 : order }} className={`movie-items ${(Object.keys(selectedMovie).length && selectedMovie.EventGroup === EventGroup) ? 'selected-movie' : ''}`}>
       <div className='image' style={{  backgroundImage: `url(${config.baseImageUrl}${EventCode}.jpg)`}}></div>
       <div className='movie-content'>
         <div className='play-button' onClick={() => handleSelectedMovie(EventGroup,order)}>
          <PlayCircleOutlineIcon fontSize="large" /> 
         </div> 
         <div className='votes'>
           <div style={{display: 'flex',justifyContent:'flex-start'}}>
            <ThumbUpIcon style={{ fill: '#5acc5a'}}/> 
           </div>
           <div className='votes-count'>
             <span>{wtsPerc}%</span> 
             <div>{wtsCount} votes</div>
           </div>
         </div>
         <div className='release-date'>
           <div className='date-circle'>
             <div className='date-content'>
             <div className='date'>{releaseDate[0].toUpperCase()}</div>
             <div className='year'>{releaseDate[1]}</div>
             </div>
           </div>
         </div>
       </div>
       <div className='footer'>
        <span className='title'>{EventTitle}</span> 
       </div>
    </div>
    </>
  )
}

export default MovieItens;

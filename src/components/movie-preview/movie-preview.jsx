import React from 'react'

import MovieItens from '../movie-items/movie-items';
import './movie-preview.scss';

const MoviePreview = ({ 
  movies, 
  handleSelectedMovie, 
  selectedMovieDetail, 
  clearMovieDetails, 
  handleClose,
  selectedGenre,
  selectedLanguage,
  search,
  selectedMovieOrder
 }) => {
  return (
    <>
    {
      (selectedGenre.length || search.length || selectedLanguage.length) &&
      <div className='applied-filters'>
        <div>Applied Filters:</div>
        {selectedGenre.length ? selectedGenre.map((item,index) => (
          <div className="chip">
            {item}
            <span className="closebtn" onClick={handleClose}>&times;</span>
          </div>
          ))
          : search.length ? 
          <div className="chip">
            {search}
            <span className="closebtn" onClick={handleClose}>&times;</span>
          </div> 
          :
          <div className="chip">
            {selectedLanguage}
            <span className="closebtn" onClick={handleClose}>&times;</span>
          </div> 
        }
        
      </div>

    }
    <div className='movie-container'>  
      {
        Object.keys(movies).map((item,index) => (
          <MovieItens 
            key={index} 
            movie={movies[item]} 
            handleSelectedMovie={handleSelectedMovie}
            selectedMovie={selectedMovieDetail}
            clearMovieDetails={clearMovieDetails}
            order={index}
            selectedMovieOrder={selectedMovieOrder}
          />
        ))
      }
    </div>
    </>
  )
}

export default MoviePreview;

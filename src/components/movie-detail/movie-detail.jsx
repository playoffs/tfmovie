import React from 'react';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import DateRangeIcon from '@material-ui/icons/DateRange';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ClearOutlinedIcon from '@material-ui/icons/ClearOutlined';

import './movie-detail.scss';

const MovieDetail = ({ 
  clearMovieDetails,
  selectedMovie,
  order
}) => {    
  const { 
    EventTitle, 
    TrailerURL, 
    EventLanguage, 
    EventGenre, 
    wtsPerc, 
    DispReleaseDate, 
    wtsCount, 
    dwtsCount, 
    maybeCount } = selectedMovie;  
  let trailerUrl = TrailerURL.replace('watch?v=','embed/');
  let finalUrl = (trailerUrl.indexOf('&') > -1) ? trailerUrl.substring(0, trailerUrl.indexOf('&')) : trailerUrl;
  let genres = EventGenre.split('|');
  let releaseDate = DispReleaseDate.split(',');
  return (
      <div className='movie-detail' style={{order: order}}>
        <div className='video'>
          <iframe width="600" height="350" title='Movie Trailer' frameBorder="0" src={finalUrl}></iframe>
        </div>
      <div className='movie-content'>
        <div className='close-btn' onClick={clearMovieDetails}><ClearOutlinedIcon style={{ fill: 'white'}} /></div>
        <div className='title'>{EventTitle}</div>
        <div className='subtitle'>{EventLanguage.toUpperCase()}</div>
        <div className='genre-section'>
          {
            genres.map((item,index) => (
              <div className='genre' key={index}>
                <span className='genre-title'>{item}</span>
              </div>
            ))
          }
        </div>
        <div className='count-section'>
          <div className='votes'>
            <div className='votes-percent'>
              <ThumbUpIcon style={{ fill: 'white'}} fontSize='small'/>
              <span style={{marginLeft: '6px'}}>{wtsPerc}%</span>
            </div>
            <div className='votes-count'>
              <span>{wtsCount} votes</span>
            </div>
          </div>
          <div className='display-date'>
            <div className='preview-date'>
              <DateRangeIcon style={{ fill: 'white'}} fontSize='medium'/>
              <span style={{marginLeft: '6px'}}>{releaseDate[0]}</span>
            </div>
            <div className='release-year'>
              <span>{releaseDate[1]}</span>
            </div>
          </div>
        </div>
        <div className='movie-footer'>
          <div className='will-watch'>
            <div className='text thumps-up-circle'>
              <ThumbUpIcon style={{ fill: '#5acc5a'}} fontSize='medium'/>
            </div> 
            <span>WILL WATCH</span>
            <span className='text'>({wtsCount})</span>
          </div>
          <div className='maybe-watch'>
            <div className='text question-mark-circle'>
              <HelpOutlineIcon style={{ fill: 'yellow'}} fontSize='medium'/>
            </div>
            <span>MAYBE WATCH</span>
            <span className='text'>({maybeCount})</span>
          </div>
          <div className='wont-watch'>
            <div className='text thumps-down-circle'>
              <ThumbDownIcon style={{ fill: '#dd5454'}} fontSize='medium'/>
            </div>  
            <span>WON'T WATCH</span>
            <span className='text'>({dwtsCount})</span>
          </div>
        </div>
      </div>
    </div>
 
  )
}

export default MovieDetail;

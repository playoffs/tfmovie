import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import Input from '@material-ui/core/Input';

import './header.scss';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
}

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  search: {
    borderColor: "#5bcd5b !important",
    color: '#5bcd5b !important',
    borderRadius: '10px !important'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  multi: {
    margin: theme.spacing(1),
    minWidth: 120,
    border: '1px solid #5bcd5b',
    borderRadius: '7px'
  }
}));

const Header = ({ 
  languageOptions, 
  handleLanguageFilter, 
  selectedLanguage, 
  selectedGenre, 
  genreOptions, 
  handleSelectedGenre,
  search,
  handleSearchChange
}) => {
  const classes = useStyles();

  const [genre, setGenre] = React.useState([]);

  const handleChange = (e) => {
    handleLanguageFilter(e.target.value)
  }

  const handleGenre = (e) => {
    setGenre(e.target.value);
    handleSelectedGenre(e.target.value)
  }


  return (
    <div className='header'>
      <span className='title'>Movie Trailers</span>
      <div className='search-box'>
        <form className={classes.root} noValidate autoComplete="off">
          <TextField id="standard-basic"  label="Search Movie" value={search} variant="outlined" size='small' onChange={handleSearchChange}/>
        </form>
      </div>
      <div className='filters'>
        <FormControl variant="outlined" className={classes.formControl} size='small' style={{borderColor: '#5bcd5b',borderRadius: '15px'}}>
          <InputLabel id="demo-simple-select-outlined-label">Selected Language</InputLabel>
            <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={selectedLanguage}
            onChange={handleChange}
            label="Select Language"
            >
              {
                languageOptions.map((item,index) => (
                  <MenuItem key={index} value={item}>{item}</MenuItem>
                ))
              }
            </Select>
        </FormControl>
        <FormControl variant='outlined' size='small' className={classes.multi}>
        <InputLabel id="demo-mutiple-checkbox-label">All Genres</InputLabel>
        <Select
          labelId="demo-mutiple-checkbox-label"
          id="demo-mutiple-checkbox"
          multiple
          value={selectedGenre}
          onChange={handleGenre}
          input={<Input />}
          renderValue={(selected) => selected.join(', ')}
          MenuProps={MenuProps}
        >
          {genreOptions.map((name,index) => (
            <MenuItem key={index} value={name}>
              <Checkbox checked={genre.indexOf(name) > -1} /> 
              <ListItemText primary={name} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      </div>
    </div>
  )
}

export default Header;
